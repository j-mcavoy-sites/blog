---
title: ProfHacks Website
subtitle: Rowan IEEE
date: 2020-02-29 00:18:17
cover_index: index.jpg
cover_detail: detail.jpg
tags:
  - project
  - web development
---

# Project Overview
ProfHacks is Rowan IEEE's annual 24-hour hackathon in which 100+ students from around the world register and attend the 2-day event. This website hosts event information, handles hacker registration, and hosts a secret "Hackenger Hunt" scavenger hunt that is incorporated in the hackathon.

## Technology and Frameworks
- Heroku - Hosting Provider
- Express.js - Backend Web Server
- Pug.js - HTML Templating Language
- Postgres - Database
- Mailgun - Emailing Service

## Code
https://gitlab.com/rowanieee/profhacks/profhacks-com
