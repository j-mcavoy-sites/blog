---
title: Clean Room Environmental Chamber
subtitle: Product Engineering Development Clinic
date: 2020-05-20 22:42:48
cover_index: index.jpg
cover_detail: detail.jpg
tags:
  - project
  - product engineering
  - electronics
  - embedded system
  - pcb design
---

# Project Overview
This project two term project and part of my Product Engineering Development senior clinic. This novel environmental chamber was designed for our client, Mario Leone, who wanted a solder paste refrigerator for the university's PCB assembly room. Mr. Leone acknowledged that he could purchase off-the-shelf products that would keep his solder paste cooled to its specified storage temperature, however he specifically wanted a refrigerator that appealed to his aesthetic sensibilities and did not look like a refrigerator -- he wanted it to look like a piece of art. Mr. Leone also wanted this refrigerator to be designed and fabricated by Rowan engineering students from scratch to demonstrate our ability to create a brand new product from concept to reality.

## What is the PEDC Clinic?
The Product Engineering Development Center (PEDC) is a small company started by Rowan University's ME and ECE technologists, Karl Dyer and Mario Leone. The PEDC is comprised of student consultants who consult with clients in the engineering college to design custom products for researchers and the department. The PEDC clinic is designed to teach students practical, hands-on engineering principles and new product development methodologies to prepare them for creating actual products in the real world.

### Problem Definition
The problems that this clinic tries to solve can be boiled down to: engineering students are all too often unable to understand the desires and sensibilities of other people, have their creativity stifled by their narrow focus on technical details, have little to no exposure into how to design finished products, and are never taught the soft skills required to successfully manage projects and turn an idea into a product on that's on market.

<!--
Karl Dyer and Mario Leone are troubled by how many engineers they see in industry who lack a
fundamental understanding of what customers want. In order to design any successful product, it is
imperative that engineers can understand their customer's sensibilities. Dyer and Leone note
that engineers often fail to think like their non-engineer customer and thus fail to design products that
consumers actually like or want to buy in the first place.

Another challenge that engineers struggle with is being able to think creatively and design products
that don't already exist. Dyer and Leone note that engineering students are rarely ever challenged to
think creatively in their courses. They argue that the lack of
-->

### Human-Centric Approach to Product Development
The PEDC clinic was designed teach engineering students how to create products and how it's not only
the "nuts and bolts" and the "arcs and the sparks" of how a product works that makes a product
successful, but more importantly the way the product will appeal to consumers. This clinic teaches a
human-centric approach to product development where the cycle begins with getting to know your
client through interviews, deciphering their need, and learning what aesthetics appeal to them and
make them happy.

The clinic derives its approach to product development from
research relating to how humans interact with products, what drives consumers to buy products, how
products make their users feel good, and universal design principles.

## Objectives
The client wants a stylish refrigerator to hold solder paste in the PCB assembly center
clean room. The

# Methodology
## Initial Client Interviews
Coming Soon

<!--
The project started with client interviews, the client explained his problem; he wanted a refrigerator that could:
- To hold at 2 tubs of solderpaste and 2 syringes
- Refrigerate the solderpaste
- Only require one hand operation
-
-->

## Aesthetic Scope of Work
Since this client's focus was the aesthetics of the environmental chamber and the desired end-user experience, we started with brainstorming different styles and images to gauge what types of art and styles the client found appealing. From these brainstorming sessions, the engineering team brought in external artist talent to design the exterior aesthetic of the chamber that aligned with the client's sensibilities.

### Technical Design
The technical design followed the initial aesthetic design. The team worked closely with the client to determine the high-level requirements for the overall system, the user interface, temperature control system, and opening-closing mechanism. Once these requirements were flushed out, the team ranked the artist concepts based on how well they suited the requirements.

# Deliverables
### Aesthetic Scope of Work
<object data="./AestheticDesignScopeDocument_1v0.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="./AestheticDesignScopeDocument_1v0.pdf" >
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="./AestheticDesignScopeDocument_1v0.pdf">Download PDF</a>.</p>
    </embed>
</object>

## Technical Scope of Work
<object data="./ScopeDocument_1v0.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="./ScopeDocument_1v0.pdf" >
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="./ScopeDocument_1v0.pdf" >Download PDF</a>.</p>
    </embed>
</object>

<!-- ### High Level Design -->

## Electrical
### PCB Schematics
Altium Designer was used to manage our PCB deign. All Altium Schematics and
layouts are available at: https://gitlab.com/A-Team-Rowan-University/pedc/cleanroomfridge/cleanroomfridge-ecad

_TODO for next semeter: the layouts for the boards are rough drafts to give the ME team an idea for the required clearences, minimum area, and airflow requirements for determining board mounting placement. These boards STEPS will need to be added to the ME assembly and revision notes will need to be supplied next design review._

### Source Code
All source code is available at: https://gitlab.com/A-Team-Rowan-University/pedc/cleanroomfridge/cleanroomfridge

_TODO for next semeter: The source code implantations need to be resumed, only the project testing, documentation, and specifications have been completed._

<!-- ## Mechanical -->


## Reports
### Final Report
<object data="./EnvironmentalChamber_Report.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="./EnvironmentalChamber_Report.pdf" >
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="./EnvironmentalChamber_Report.pdf">Download PDF</a>.</p>
    </embed>
</object>


# Credits
## Clinic Advisors
* [Mario Leone](https://www.linkedin.com/in/mario-leone-39494a4/), ECE Technologist
* Karl Dyer, ME Technologist

## Clinic Team
*Electrical:*
* [Anwar Hussein](https://www.linkedin.com/in/anwar-hussein-645b96140/)
* [Armando Briscella](https://www.linkedin.com/in/armando-briscella-a0b609172/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_top%3B5HTS5GBeQbKXdhSs0WKd5w%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_top-search_srp_result&lici=sL%2FdUI1qRUaWXVsQbF%2FQ%2FQ%3D%3D)
* [James Merrill](https://www.linkedin.com/in/james-merrill-15275214b/)
* [John McAvoy](https://www.linkedin.com/in/john-mcavoy-8b888b183/)
* [Julia Konstantinos](https://www.linkedin.com/in/julia-konstantinos/)
* [Karlie Naphy](https://www.linkedin.com/in/karlie-naphy/)
* [Kimberly Tran](https://www.linkedin.com/in/kimberly-j-tran/)
* [Tim Gayed](https://www.linkedin.com/in/tim-gayed/)
* [Tim Hollabaugh](https://www.linkedin.com/in/tim-hollabaugh/)

*Mechanical:*
* [Carla Silvestri](https://www.linkedin.com/in/carla-e-silvestri/)
* [Caroline Thistle](https://www.linkedin.com/in/caroline-g-thistle/)
* [Gabriel Bateman](https://www.linkedin.com/in/gabriel-bateman-b8136a14a/)
* [Noah Ng](https://www.linkedin.com/in/noah-ng-618099185/)
* [Sara Toner](https://www.linkedin.com/in/sara-toner-engineer/)
