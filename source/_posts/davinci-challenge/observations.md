---
title: 'DaVinci Challange Observations: 2020-07-31'
tags:
  - DaVinci Challenge
date: 2020-07-31 04:32:06
---

# Observations
## Q1: How are tea kettles made?

## Q2: How is camomile tea made?
{% youtube wSJwjBGNh_I %}
{% youtube KOpxSbslpYU %}
## Q3: How are guitar strings made?
{% youtube sHoyP53naNk %}
{% youtube 4_MVOZnhLVc %}
## Q4: How does E-Ink work?
## Q5: What is the environmental impact of Kureg machines?
## Q6: How is toothpaste made and packaged?
## Q7: How are pillows made?
## Q8: How do CDs work?
## Q9: What regulations and standards specify how sidewalks are made?
## Q10: How are scents combined and made?
