---
title: antboat
cover_index: index.jpg
cover_detail: detail.jpg
tags: project
date: 2020-08-19 22:51:08
subtitle: Personal Project
---

# Project Overview
Antboat is commandline migration tool, written in Rust, for syncing podcasts and playback between AntennaPod Android App and Newsboat/Podboat.

## Code
https://gitlab.com/j-mcavoy/antboat
