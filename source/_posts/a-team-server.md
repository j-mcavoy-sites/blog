---
title: Resource Website
date: 2020-08-19 19:58:07
subtitle: A-Team
tags: project
cover_index: index.jpg
cover_detail: detail.jpg
---

# Project Overview
The A-Team resource management web application was for the Rowan University Engineering Department as well as for the A-Team's internal management. Web services were developed for:
- Managing the A-Team
  - Managing A-Team employees
  - Managing the ECE Resource Center Inventory and Equipment Loans
- Managing Rowan Engineering students
  - Creating an internal web-based safety training program for all Rowan Engineering students, specific to the labs and Equipment in the College of Engineering
  - An equipment lockout system for enforcing student safety training compliance on all heavy machinery
  - Integrating with student ID cards to sign out equipment

## Technology and Frameworks used
### 2017
- Google scripts - For generating and grading Google Form lab safety tests

### 2018
- Diesel.rs backend - Database ORM written in Rust
- CentOS - Running on a self hosted company server. Runs all of the Docker and Kubernetes services.
- MySQL database
- Docker
- Kubernetes - GitLab runner integration with GitLab CI/CD for rapidly building pipelines
- React.js frontend

### 2019
- Elm.js frontend - Functional programming language for developing fast Model, Update, View
- Python scripts - for reading student ID cards
- Postgres database

### 2020
- Django - Python full-stack web framework for scaling all of the separate projects into their own microservices

## Source Code
https://gitlab.com/A-Team-Rowan-University/web-development/a-team-website

https://github.com/A-Team-Rowan-University/resource-website-backend

https://github.com/A-Team-Rowan-University/resource-website-frontend/tree/master/frontend/elm-src

https://github.com/A-Team-Rowan-University/people_database

https://github.com/A-Team-Rowan-University/main-server

https://github.com/A-Team-Rowan-University/a-team-website

https://gitlab.com/A-Team-Rowan-University/web-development/resource-website
