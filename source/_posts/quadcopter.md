---
title: Quadcopter
subtitle: Rowan RAS
date: 2019-08-01 00:10:14
cover_index: index.jpg
cover_detail: detail.jpg
tags: project
---

# Project Overview
Fully autonomous quadcopter for AUVSI SUAS Competition.

# Promotional Video
{% youtube tpPKivjax1Y %}
<br>

## Competition Qualifying Proof of Flight
{% youtube pKyw97Qs5wM %}
<br>
{% youtube QWyAu4B1cPQ %}
<br>

## Competition
{% asset_img suas_ras_grup_photo.jpg 600 %}
Rowan RAS Group Photo

{% asset_img suas_group_photo.jpg 600 %}
2018 AUVSI SUAS Competition Group Photo
