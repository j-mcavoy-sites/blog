---
title: Screaming Panda Guitar Pedal
subtitle:
date: 2020-07-27 14:51:52
cover_index: #index.jpg
cover_detail: #detail.jpg
tags:
  - project
  - electronics
  - pcb design
  - audio
  - Solidworks
  - Altium
  - KiCad
  - Onshape
---

# Project Overview
DIY Tube Screamer pedal for guitar and bass.

## How to
Coming Soon!

## Project Files
https://gitlab.com/j-mcavoy/screaming-panda
