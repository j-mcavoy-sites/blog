---
title: Smart Mirror
date: 2016-01-16 14:55:17
subtitle: Personal Project
cover_index: index.jpg
cover_detail: overview.jpg
tags: project
#photos:
#  - weather.jpg
#  - overview.jpg
#  - side2.jpg
#  - time.jpg
#  - face.jpg
---

# Project Overview
A DIY RaspberryPi powered Smart Mirror.

### How To
#### Bill of Materials
- RaspberryPi, any model will work, if you use a Pi Zero you will need an HDMI-miniHDMI adapter and if your model does not have WiFi, you will need an approbate adapter.
- MicroSD card, 8GB is plenty
- (Recommended) Pi Case
- 2-way mirror: [Acrylic](https://www.amazon.com/0-04-Acrylic-See-Through-Mirror-Transparent/dp/B017ONH3EG/ref=sr_1_2?dchild=1&keywords=2+way+acrylic+mirror&qid=1597879109&sr=8-2) (cheaper) or [Glass](https://www.amazon.com/Two-Way-Glass-Mirror-Surveillance/dp/B07173L96J/ref=sr_1_4?dchild=1&keywords=2%2Bway%2Bacrylic%2Bmirror&qid=1597879109&sr=8-4&th=1) (more expensive)
- Cheap Monitor (with HDMI Input)
- HDMI Cable
- Brackets (to mount the monitor inside the frame) I 3D printed mine [](https://www.thingiverse.com/thing:3005559)
- Wood
- Wood Stain
- Sandpaper
- Wood Glue

#### Assembly

{% youtube fkVBAcvbrjU?t=40 %}


### Raspberry Pi Setup
```bash
sudo apt -y update && sudo apt -y upgrade
sudo apt install -y git python python-imaging-tk
```

### Code
For this project, I forked [HackerShackOfficial's python smart mirror code](https://github.com/HackerShackOfficial/Smart-Mirror) to get started.
```bash
git clone https://github.com/j-mcavoy/smart_mirror
cd smart_mirror
pip install -r requirements.txt
```

Replace your weather api in `smartmirror.py`

{% codeblock smartmirror.py lang:python https://github.com/j-mcavoy/smart_mirror/blob/master/smartmirror.py source first_line:44 %}
news_country_code = 'us'
weather_api_token = 'Insert api key here' # create account at https://darksky.net/dev/
weather_lang = 'en' # see https://darksky.net/dev/docs/forecast for full list of language parameters values
weather_unit = 'us' # see https://darksky.net/dev/docs/forecast for full list of unit parameters values
{% endcodeblock %}

Run
```bash
python smartmirror.py
```


#### Frameworks and Libraries
The frameworks I used were:
* [Tkinter](https://docs.python.org/3/library/tkinter.html) - Desktop Application framework
* [DarkSky](https://darksky.ne) - Weather API
* [OpenCV](https://pypi.org/project/opencv-python/) - Computer vision library


## Next Steps

The next steps for this project is to package the RaspPi image using [Yocto](https://www.yoctoproject.org/) build tools( I'll make a separate tutorial for that)

{% asset_img image side.jpg 200 %}
{% asset_img image weather.jpg 200 %}
{% asset_img image time.jpg 200 %}
{% asset_img image face.jpg 200 %}

## Source Code
* [j-mcavoy/smart_mirror](https://github.com/j-mcavoy/smart_mirror)

## Credits
* [Hacker Shack](https://github.com/HackerShackOfficial/Smart-Mirror)
* [Hacker House Video](https://www.youtube.com/watch?v=fkVBAcvbrjU)
