---
title: About Me
photos:
  - images/profilepic.jpg
  - images/panda_icon.png
  - images/profilepic.jpg
  - images/profilepic.jpg
---

My name is John McAvoy, I am a 22 year-old electrical and computer engineer
interested in software engineering, full-stack web development, Linux
development, PCB design, and product engineering.

## Contact Me
Email: j-mcavoy@protonmail.com
